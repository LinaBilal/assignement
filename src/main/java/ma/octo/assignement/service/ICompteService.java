package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;

import java.util.List;

public interface ICompteService {
    List<Compte> findAll();
    Compte findByNrCompte(String nrCompteEmetteur);
    Compte save(Compte compte);
    Compte findByRib(String RibBeneficiaire);
}
