package ma.octo.assignement.service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.List;

public interface IVersementService
{
    List<Versement> findAll();
    Versement save(Versement versement);
    Versement save(VersementDto versementDto);
    void processVersement(VersementDto versementDto, int MONTANT_MAXIMAL) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;





}
