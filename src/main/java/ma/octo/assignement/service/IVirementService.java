package ma.octo.assignement.service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IVirementService {
    List<Virement> findAll();
    Virement save(Virement virement);
    Virement save(VirementDto virementDto);
    void processVirement(VirementDto virementDto, int MONTANT_MAXIMAL) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;
}
