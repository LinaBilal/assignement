package ma.octo.assignement.service.imp;

import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditVersementRepository;
import ma.octo.assignement.repository.AuditVirementRepository;
import ma.octo.assignement.service.IAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuditService implements IAuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    private AuditVirementRepository auditVirementRepository;
    private AuditVersementRepository auditVersementRepository;
    @Autowired
    public AuditService(AuditVirementRepository auditVirementRepository, AuditVersementRepository auditVersementRepository) {
        this.auditVirementRepository = auditVirementRepository;
        this.auditVersementRepository = auditVersementRepository;
    }

    public void auditVirement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VIREMENT);

        AuditVirement audit = new AuditVirement();
        audit.setEventType(EventType.VIREMENT);
        audit.setMessage(message);
        auditVirementRepository.save(audit);
    }


    public void auditVersement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VERSEMENT);

        AuditVirement audit = new AuditVirement();
        audit.setEventType(EventType.VERSEMENT);
        audit.setMessage(message);
        auditVersementRepository.save(audit);
    }
}
