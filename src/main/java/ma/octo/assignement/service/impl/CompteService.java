package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompteService implements ICompteService {

    CompteRepository compteRepository;

    @Autowired
    public CompteService(CompteRepository compteRepository) { this.compteRepository = compteRepository; }

    @Override
    public List<Compte> findAll() { return compteRepository.findAll(); }

    @Override
    public Compte findByNrCompte(String nrCompteEmetteur) { return compteRepository.findByNrCompte(nrCompteEmetteur); }

    @Override
    public Compte save(Compte compte) { return compteRepository.save(compte); }

    @Override
    public Compte findByRib(String RibBeneficiaire) { return compteRepository.findByRib(RibBeneficiaire); }
}
