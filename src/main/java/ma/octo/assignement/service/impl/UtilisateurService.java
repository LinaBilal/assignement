package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UtilisateurService implements IUtilisateurService {

    UtilisateurRepository utilisateurRepository;

    @Autowired
    public UtilisateurService(UtilisateurRepository utilisateurRepository) { this.utilisateurRepository = utilisateurRepository; }
    @Override
    public List<Utilisateur> findAll() { return utilisateurRepository.findAll(); }

    @Override
    public Utilisateur findByUsername(String username) { return utilisateurRepository.findByUsername(username); }

    @Override
    public Utilisateur save(Utilisateur utilisateur) { return null; }
}
