package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.IVersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VersementService implements IVersementService {

    Logger LOGGER = LoggerFactory.getLogger(VersementService.class);

    VersementRepository versementRepository;

    CompteRepository compteRepository;

    @Autowired
    public VersementService(VersementRepository versementRepository,CompteRepository compteRepository) {
        this.versementRepository = versementRepository;
        this.compteRepository = compteRepository;
    }

    @Override
    public List<Versement> findAll()
    {
        return versementRepository.findAll();
    }

    @Override
    public Versement save(Versement versement)
    {
        return versementRepository.save(versement);
    }

    @Override
    public Versement save(VersementDto versementDto) {
        Compte c = compteRepository.findByRib(versementDto.getRib());

        Versement versement = new Versement();

        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setCompteBeneficiaire(c);
        versement.setNom_prenom_emetteur(versementDto.getNomEmetteur());
        versement.setDateExecution(versementDto.getDate());
        return versementRepository.save(versement);

    }

    @Override
    public void processVersement(VersementDto versementDto, int MONTANT_MAXIMAL) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Compte c = compteRepository.findByRib(versementDto.getRib());
        this.checkVersement(versementDto,c,MONTANT_MAXIMAL);

        c.setSolde(new BigDecimal(c.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        compteRepository.save(c);

    }

    private void checkMontant(BigDecimal montantVersement, int MONTANT_MAXIMAL) throws TransactionException
    {
        if (montantVersement == null || montantVersement.intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (montantVersement.intValue() < 10) {
            LOGGER.error("Montant minimal de versement non atteint");
            throw new TransactionException("Montant minimal de versement non atteint");
        } else if (montantVersement.intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de versement dépassé");
            throw new TransactionException("Montant maximal de versement dépassé");
        }
    }

    private void checkVersement(VersementDto versementDto, Compte c, int MONTANT_MAXIMAL) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        c = compteRepository.findByRib(versementDto.getRib());

        if (c == null)
        {
            LOGGER.error("Compte Non existant [" + versementDto.getRib() + "]");
            throw new CompteNonExistantException("Compte Non existant [" + versementDto.getRib() + "]");
        }

        this.checkMontant(versementDto.getMontantVersement(), MONTANT_MAXIMAL);

        if (versementDto.getMotifVersement() == null || versementDto.getMotifVersement().length() == 0) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (c.getSolde() == null || c.getSolde().intValue() - versementDto.getMontantVersement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }
    }

}

