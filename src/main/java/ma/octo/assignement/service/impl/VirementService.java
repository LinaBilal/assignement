package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.IVirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class VirementService implements IVirementService {

    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);

    VirementRepository virementRepository;

    CompteRepository compteRepository;

    @Autowired
    public VirementService(VirementRepository virementRepository, CompteRepository compteRepository) {
        this.virementRepository = virementRepository;
        this.compteRepository = compteRepository;
    }

    @Override
    public List<Virement> findAll() {
        return virementRepository.findAll();
    }

    @Override
    public Virement save(Virement virement) {
        return virementRepository.save(virement);
    }

    @Override
    public Virement save(VirementDto virementDto) {
        Virement virement = new Virement();
        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBenificiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(compteBenificiaire);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMotifVirement(virementDto.getMotif());
        virement.setMontantVirement(virementDto.getMontantVirement());

        return virementRepository.save(virement);
    }

    @Override
    public void processVirement(VirementDto virement, int MONTANT_MAXIMAL) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        Compte compteEmetteur = compteRepository.findByNrCompte(virement.getNrCompteEmetteur());
        Compte compteBenificiaire = compteRepository.findByNrCompte(virement.getNrCompteBeneficiaire());

        this.checkVirement(virement, compteEmetteur, compteBenificiaire, MONTANT_MAXIMAL);

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virement.getMontantVirement()));
        compteRepository.save(compteEmetteur);

        compteBenificiaire
                .setSolde(new BigDecimal(compteBenificiaire.getSolde().intValue() + virement.getMontantVirement().intValue()));
        compteRepository.save(compteBenificiaire);
    }

    private void checkVirement(VirementDto virement, Compte compteEmetteur, Compte compteBenificiaire, int MONTANT_MAXIMAL) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        if (compteEmetteur == null) {
            LOGGER.error("Compte Emetteur Non existant [" + virement.getNrCompteEmetteur() + "]");
            throw new CompteNonExistantException("Compte Emetteur Non existant [" + virement.getNrCompteEmetteur() + "]");
        }

        if (compteBenificiaire == null) {
            LOGGER.error("Compte Benificiare Non existant [" + virement.getNrCompteBeneficiaire() + "]");
            throw new CompteNonExistantException("Compte Non existant [" + virement.getNrCompteBeneficiaire() + "]");
        }

        this.checkMontant(virement.getMontantVirement(), MONTANT_MAXIMAL);

        if (virement.getMotif() == null || virement.getMotif().length() == 0) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compteEmetteur.getSolde() == null || compteEmetteur.getSolde().intValue() - virement.getMontantVirement().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }
    }

    private void checkMontant(BigDecimal montant, int MONTANT_MAXIMAL) throws TransactionException {
        if (montant == null || montant.intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (montant.intValue() < 10) {
            LOGGER.error("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (montant.intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }
    }
}
