package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IUtilisateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/comptes")
public class CompteController {

    Logger LOGGER = LoggerFactory.getLogger(CompteController.class);

    private ICompteService compteService;


    @Autowired
    public CompteController(ICompteService compteService) {
        this.compteService = compteService;
    }

    @GetMapping
    List<Compte> loadAll() {
        List<Compte> all = compteService.findAll();
        LOGGER.info("Get All Comptes Called, {} returned", all.size());
        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }
}
