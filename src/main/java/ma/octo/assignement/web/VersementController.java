package ma.octo.assignement.web;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IUtilisateurService;
import ma.octo.assignement.service.IVersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/versements")
class VersementController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    private ICompteService compteService;

    private IVersementService versementService;

    private IAuditService auditService;

    private IUtilisateurService utilisateurService;

    @Autowired
    public VersementController(ICompteService compteService, IVersementService versementService, IAuditService auditService, IUtilisateurService utilisateurService) {
        this.compteService = compteService;
        this.versementService = versementService;
        this.auditService = auditService;
        this.utilisateurService = utilisateurService;
    }

    @GetMapping
    List<Versement> loadAll() {
        LOGGER.info("Get All Versements Called");
        List<Versement> all = versementService.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody VersementDto versementDto)
            throws CompteNonExistantException, SoldeDisponibleInsuffisantException, TransactionException
    {

        this.versementService.processVersement(versementDto,this.MONTANT_MAXIMAL);
        versementService.save(versementDto);
        auditService.auditVersement("Versement depuis : " +versementDto.getNomEmetteur() + " vers "+ versementDto.
                getRib() +" d'un montant de" +versementDto.getMontantVersement()
                .toString());
    }
}
