package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IUtilisateurService;
import ma.octo.assignement.service.IVirementService;
import ma.octo.assignement.service.impl.VirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;


@RestController
@RequestMapping(value = "/virements")
class VirementController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);


    private ICompteService compteService;

    private IVirementService virementService;

    private IAuditService auditService;

    private IUtilisateurService utilisateurService;


    @Autowired
    public VirementController(IAuditService auditService, ICompteService compteService, IVirementService virementService, IUtilisateurService utilisateurService) {
        this.auditService = auditService;
        this.compteService = compteService;
        this.virementService = virementService;
        this.utilisateurService = utilisateurService;
    }


    @GetMapping
    List<Virement> loadAll() {
        List<Virement> all = virementService.findAll();
        LOGGER.info("Get All Virements Called, {} returned", all.size());

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        this.virementService.processVirement(virementDto, this.MONTANT_MAXIMAL);
        virementService.save(virementDto);
        auditService.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString());

    }

}
