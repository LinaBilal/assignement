package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.impl.VersementService;
import ma.octo.assignement.service.impl.VirementService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class VersementServiceTest {
    VersementRepository versementRepository = Mockito.mock(VersementRepository.class);
    CompteRepository compteRepository = Mockito.mock(CompteRepository.class);
    IVersementService versementService;


    public VersementServiceTest() { versementService = new VersementService(versementRepository,compteRepository); }

    @Test
    public void findAll_should_return_list() {
        // arrange
        Versement v1 = new Versement();
        v1.setId(1L);
        v1.setMontantVersement(new BigDecimal(100));
        v1.setMotifVersement("Versement de test");

        Versement v2 = new Versement();
        v2.setId(2L);
        v2.setMontantVersement(new BigDecimal(200));
        v2.setMotifVersement("Versement de test");

        List<Versement> listMocked = new ArrayList<>();
        listMocked.add(v1);
        listMocked.add(v2);

        // mock
        Mockito.when(versementRepository.findAll()).thenReturn(listMocked);

        // execute
        List<Versement> listReturned = this.versementService.findAll();

        // assert
        Assertions.assertEquals(listReturned.size(), listMocked.size());
        Assertions.assertNotNull(listReturned.get(0));
        Assertions.assertEquals(listReturned.get(0), listMocked.get(0));
    }

    @Test
    public void save_versementDto_should_return_versement() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(100));
        versementDto.setMotifVersement("Virement de test");
        versementDto.setRib("A0000425B14440");
        versementDto.setNomEmetteur("Lina Bilal");


        Versement versement = new Versement();
        versement.setId(1L);
        versement.setMontantVersement(new BigDecimal(100));
        versement.setMotifVersement("Versement de test");

        // mock
        Mockito.when(compteRepository.findByRib(Mockito.anyString())).thenReturn(new Compte());
        Mockito.when(versementRepository.save(Mockito.any())).thenReturn(versement);

        // execute
        Versement versementReturned = this.versementService.save(versementDto);

        // assert
        Assertions.assertEquals(versement, versementReturned);
    }

    @Test
    public void processVersement_with_no_rib_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(100));
        versementDto.setMotifVersement("Virement de test");
        versementDto.setRib("15154A02052B44");
        versementDto.setNomEmetteur("OCTO STAGE");

        // mock
        Mockito.when(compteRepository.findByRib("C1")).thenReturn(null);

        // execute & assert
        Assertions.assertThrows(CompteNonExistantException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_no_compte_beneficiaire_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(100));
        versementDto.setMotifVersement("Virement de test");
        versementDto.setRib("A10045B1000");
        versementDto.setNomEmetteur("LINA BIL");

        // mock
        Mockito.when(compteRepository.findByRib("C1")).thenReturn(new Compte());
        Mockito.when(compteRepository.findByNrCompte("C2")).thenReturn(null);

        // execute & assert
        Assertions.assertThrows(CompteNonExistantException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_null_montant_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(null);

        // mock
        Mockito.when(compteRepository.findByRib(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_montant_equals_zero_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(0));

        // mock
        Mockito.when(compteRepository.findByRib(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_montant_less_than_10_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(5));

        // mock
        Mockito.when(compteRepository.findByRib(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_montant_more_than_max_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(1001));

        // mock
        Mockito.when(compteRepository.findByRib(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_no_motif_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(100));
        versementDto.setMotifVersement("");
        // mock
        Mockito.when(compteRepository.findByRib(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_null_sold_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(100));
        versementDto.setMotifVersement("motif");
        // mock
        Mockito.when(compteRepository.findByRib(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(SoldeDisponibleInsuffisantException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_not_enough_sold_should_throw_error() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(100));
        versementDto.setMotifVersement("motif");

        Compte compte = new Compte();
        compte.setSolde(new BigDecimal(90));
        // mock
        Mockito.when(compteRepository.findByRib(Mockito.any())).thenReturn(compte);

        // execute & assert
        Assertions.assertThrows(SoldeDisponibleInsuffisantException.class, () -> {
            this.versementService.processVersement(versementDto, 1000);
        });
    }

    @Test
    public void processVersement_with_valid_versement_should_pass() {
        // arrange
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(new BigDecimal(100));
        versementDto.setMotifVersement("motif");
        versementDto.setRib("1000A0010B");
        versementDto.setNomEmetteur("Nom emetteur");

        Compte c = new Compte();
        c.setSolde(new BigDecimal(100));

        // mock
        Mockito.when(compteRepository.findByRib("1000A0010B")).thenReturn(c);
        Mockito.when(compteRepository.save(c)).thenReturn(c);

        // execute
        try {
            this.versementService.processVersement(versementDto, 1000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // assert
        Assertions.assertEquals(c.getSolde(), new BigDecimal(200));

    }


}
