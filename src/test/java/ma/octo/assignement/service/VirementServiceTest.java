package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.impl.VirementService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class VirementServiceTest {
    VirementRepository virementRepository = Mockito.mock(VirementRepository.class);
    CompteRepository compteRepository = Mockito.mock(CompteRepository.class);
    IVirementService virementService;

    public VirementServiceTest() {
        virementService = new VirementService(virementRepository, compteRepository);
    }

    @Test
    public void findAll_should_return_list() {
        // arrange
        Virement v1 = new Virement();
        v1.setId(1L);
        v1.setMontantVirement(new BigDecimal(100));
        v1.setMotifVirement("Virement de test");

        Virement v2 = new Virement();
        v2.setId(2L);
        v2.setMontantVirement(new BigDecimal(200));
        v2.setMotifVirement("Virement de test");

        List<Virement> listMocked = new ArrayList<>();
        listMocked.add(v1);
        listMocked.add(v2);

        // mock
        Mockito.when(virementRepository.findAll()).thenReturn(listMocked);

        // execute
        List<Virement> listReturned = this.virementService.findAll();

        // assert
        Assertions.assertEquals(listReturned.size(), listMocked.size());
        Assertions.assertNotNull(listReturned.get(0));
        Assertions.assertEquals(listReturned.get(0), listMocked.get(0));
    }

    @Test
    public void save_virementDto_should_return_virement() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setMotif("Virement de test");
        virementDto.setNrCompteBeneficiaire("C1");
        virementDto.setNrCompteEmetteur("C2");

        Virement virement = new Virement();
        virement.setId(1L);
        virement.setMontantVirement(new BigDecimal(100));
        virement.setMotifVirement("Virement de test");

        // mock
        Mockito.when(compteRepository.findByNrCompte(Mockito.anyString())).thenReturn(new Compte());
        Mockito.when(virementRepository.save(Mockito.any())).thenReturn(virement);

        // execute
        Virement virementReturned = this.virementService.save(virementDto);

        // assert
        Assertions.assertEquals(virement, virementReturned);
    }

    @Test
    public void processVirement_with_no_compte_emmeteur_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setMotif("Virement de test");
        virementDto.setNrCompteBeneficiaire("C1");
        virementDto.setNrCompteEmetteur("C2");

        // mock
        Mockito.when(compteRepository.findByNrCompte("C1")).thenReturn(null);
        Mockito.when(compteRepository.findByNrCompte("C2")).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(CompteNonExistantException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_no_compte_beneficiaire_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setMotif("Virement de test");
        virementDto.setNrCompteBeneficiaire("C1");
        virementDto.setNrCompteEmetteur("C2");

        // mock
        Mockito.when(compteRepository.findByNrCompte("C1")).thenReturn(new Compte());
        Mockito.when(compteRepository.findByNrCompte("C2")).thenReturn(null);

        // execute & assert
        Assertions.assertThrows(CompteNonExistantException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_null_montant_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(null);

        // mock
        Mockito.when(compteRepository.findByNrCompte(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_montant_equals_zero_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(0));

        // mock
        Mockito.when(compteRepository.findByNrCompte(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_montant_less_than_10_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(5));

        // mock
        Mockito.when(compteRepository.findByNrCompte(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_montant_more_than_max_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(1001));

        // mock
        Mockito.when(compteRepository.findByNrCompte(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_no_motif_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setMotif("");
        // mock
        Mockito.when(compteRepository.findByNrCompte(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(TransactionException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_null_sold_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setMotif("motif");
        // mock
        Mockito.when(compteRepository.findByNrCompte(Mockito.any())).thenReturn(new Compte());

        // execute & assert
        Assertions.assertThrows(SoldeDisponibleInsuffisantException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_not_enough_sold_should_throw_error() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setMotif("motif");

        Compte compte = new Compte();
        compte.setSolde(new BigDecimal(90));
        // mock
        Mockito.when(compteRepository.findByNrCompte(Mockito.any())).thenReturn(compte);

        // execute & assert
        Assertions.assertThrows(SoldeDisponibleInsuffisantException.class, () -> {
            this.virementService.processVirement(virementDto, 1000);
        });
    }

    @Test
    public void processVirement_with_valid_virement_should_pass() {
        // arrange
        VirementDto virementDto = new VirementDto();
        virementDto.setMontantVirement(new BigDecimal(100));
        virementDto.setMotif("motif");
        virementDto.setNrCompteBeneficiaire("C1");
        virementDto.setNrCompteEmetteur("C2");

        Compte compteEmetteur = new Compte();
        compteEmetteur.setSolde(new BigDecimal(110));

        Compte compteBenificiaire = new Compte();
        compteBenificiaire.setSolde(new BigDecimal(10));

        // mock
        Mockito.when(compteRepository.findByNrCompte("C1")).thenReturn(compteBenificiaire);
        Mockito.when(compteRepository.findByNrCompte("C2")).thenReturn(compteEmetteur);
        Mockito.when(compteRepository.save(compteBenificiaire)).thenReturn(compteBenificiaire);
        Mockito.when(compteRepository.save(compteEmetteur)).thenReturn(compteEmetteur);

        // execute
        try {
            this.virementService.processVirement(virementDto, 1000);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // assert
        Assertions.assertEquals(compteBenificiaire.getSolde(), new BigDecimal(110));
        Assertions.assertEquals(compteEmetteur.getSolde(), new BigDecimal(10));

    }

}
